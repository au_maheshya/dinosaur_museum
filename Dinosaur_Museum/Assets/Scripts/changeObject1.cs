using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeObject1 : MonoBehaviour

{

    public GameObject obj1;
    public GameObject obj2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void changeObj(int x)
    {

        if (x == 0)
        {
            obj1.SetActive(true);
            obj2.SetActive(false);
        }
        else
        {
            obj2.SetActive(true);
            obj1.SetActive(false);
        }
    }
}
