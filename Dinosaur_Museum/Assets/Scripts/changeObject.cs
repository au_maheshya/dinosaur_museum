using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeObject : MonoBehaviour
{

    public GameObject skeleton;
    public GameObject skinmesh;

    public GameObject button1;
    public GameObject button2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void changeobj(int era)
    {
        if (era ==0)
        {
            skeleton.SetActive(false);
            skinmesh.SetActive(true);
            button1.SetActive(false);
            button2.SetActive(true);



        }
        else
        {
            skeleton.SetActive(true);
            skinmesh.SetActive(false);
            button2.SetActive(false);
            button1.SetActive(true);


        }
        
    } 
}
