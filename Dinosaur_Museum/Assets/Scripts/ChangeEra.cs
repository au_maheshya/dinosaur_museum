using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeEra : MonoBehaviour
{
    public GameObject skeleton;
    public GameObject skinned;

    public GameObject nowButton;
    public GameObject thenButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


   public void changeobject(int era)
    {
        if (era == 0)
        {

            skeleton.SetActive(true);
            skinned.SetActive(false);

            nowButton.SetActive(false);
            thenButton.SetActive(true);

        }
            
        else
        {

            skeleton.SetActive(false);
            skinned.SetActive(true);

            nowButton.SetActive(true);
            thenButton.SetActive(false);
        }


    }
}
