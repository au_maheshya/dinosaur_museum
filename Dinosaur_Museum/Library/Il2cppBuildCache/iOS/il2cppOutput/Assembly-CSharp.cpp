﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_tCB3D927F30A1769FAEA216264EE98EFFDA4E5DF2;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// AllosaurusCameraScript
struct AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2;
// AllosaurusCameraScript2
struct AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C;
// AllosaurusSelectScript
struct AllosaurusSelectScript_t55B7409202A46909BC18D0919BC5D6A4FB005ACA;
// UnityEngine.AnimationClip
struct AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// ChangeEra
struct ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// SimpleAllosaurusCharacter
struct SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B;
// SimpleAllosaurusUserController
struct SimpleAllosaurusUserController_t78C0DD7D74194E7802BE8D8EF95FA4DD4F50A3FE;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// changeObject
struct changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF;
// myARScript
struct myARScript_tEE07E96B5A96A03A733B48DA96263E6D53BE20E5;

IL2CPP_EXTERN_C String_t* _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral429F4C6175C380F918ECE9198122F191B47F685D;
IL2CPP_EXTERN_C String_t* _stringLiteral508450355DF2A5BB172C6D21A5C54D7806587D91;
IL2CPP_EXTERN_C String_t* _stringLiteral539886CC0D6E626CD59CCCB9A251E15C3F8DC3EE;
IL2CPP_EXTERN_C String_t* _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C;
IL2CPP_EXTERN_C String_t* _stringLiteral724F6EF07C0D3FFFD32A55027218B65FB4F377E1;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7;
IL2CPP_EXTERN_C String_t* _stringLiteral8FAFA495180941C2D366DAA66DEA6A068EE6CC9F;
IL2CPP_EXTERN_C String_t* _stringLiteral9DF28E2C80756BFACBF3215EA21F094B08573DA8;
IL2CPP_EXTERN_C String_t* _stringLiteralC91F9EB420C3FF6B64C07666E3FCAD60BE80E5D7;
IL2CPP_EXTERN_C String_t* _stringLiteralDACCF1FF5F51F90AD8CA6644E88C06E5B9441FFB;
IL2CPP_EXTERN_C String_t* _stringLiteralFBC1FBDF3F91C0637B6624C6C526B3718C7E46A2;
IL2CPP_EXTERN_C String_t* _stringLiteralFC6687DC37346CD2569888E29764F727FAF530E0;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B_mD0D1DB7A2460FDDE4787FA18F91F720B574AFD05_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisAllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2_m5A589ED476E3CEB37D4A568C8D5AFB9935865C24_RuntimeMethod_var;

struct AnimatorClipInfoU5BU5D_tCB3D927F30A1769FAEA216264EE98EFFDA4E5DF2;
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AnimatorClipInfo
struct AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610 
{
public:
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;

public:
	inline static int32_t get_offset_of_m_ClipInstanceID_0() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610, ___m_ClipInstanceID_0)); }
	inline int32_t get_m_ClipInstanceID_0() const { return ___m_ClipInstanceID_0; }
	inline int32_t* get_address_of_m_ClipInstanceID_0() { return &___m_ClipInstanceID_0; }
	inline void set_m_ClipInstanceID_0(int32_t value)
	{
		___m_ClipInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_m_Weight_1() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610, ___m_Weight_1)); }
	inline float get_m_Weight_1() const { return ___m_Weight_1; }
	inline float* get_address_of_m_Weight_1() { return &___m_Weight_1; }
	inline void set_m_Weight_1(float value)
	{
		___m_Weight_1 = value;
	}
};


// UnityEngine.AnimatorStateInfo
struct AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.ForceMode
struct ForceMode_t7778317A4C46140D50D98811D65A7B22E38163D5 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ForceMode_t7778317A4C46140D50D98811D65A7B22E38163D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Motion
struct Motion_t3EAEF01D52B05F10A21CC9B54A35C8F3F6BA3A67  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.AnimationClip
struct AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178  : public Motion_t3EAEF01D52B05F10A21CC9B54A35C8F3F6BA3A67
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// AllosaurusCameraScript
struct AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject AllosaurusCameraScript::target
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___target_4;
	// System.Single AllosaurusCameraScript::turnSpeed
	float ___turnSpeed_5;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2, ___target_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_target_4() const { return ___target_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_4), (void*)value);
	}

	inline static int32_t get_offset_of_turnSpeed_5() { return static_cast<int32_t>(offsetof(AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2, ___turnSpeed_5)); }
	inline float get_turnSpeed_5() const { return ___turnSpeed_5; }
	inline float* get_address_of_turnSpeed_5() { return &___turnSpeed_5; }
	inline void set_turnSpeed_5(float value)
	{
		___turnSpeed_5 = value;
	}
};


// AllosaurusCameraScript2
struct AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject AllosaurusCameraScript2::target
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___target_4;
	// System.Single AllosaurusCameraScript2::turnSpeed
	float ___turnSpeed_5;
	// UnityEngine.GameObject AllosaurusCameraScript2::allosaurusCamera
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___allosaurusCamera_6;
	// System.Single AllosaurusCameraScript2::cameraAngleX
	float ___cameraAngleX_7;
	// System.Single AllosaurusCameraScript2::cameraAngleY
	float ___cameraAngleY_8;
	// System.Single AllosaurusCameraScript2::cameraDistance
	float ___cameraDistance_9;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C, ___target_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_target_4() const { return ___target_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_4), (void*)value);
	}

	inline static int32_t get_offset_of_turnSpeed_5() { return static_cast<int32_t>(offsetof(AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C, ___turnSpeed_5)); }
	inline float get_turnSpeed_5() const { return ___turnSpeed_5; }
	inline float* get_address_of_turnSpeed_5() { return &___turnSpeed_5; }
	inline void set_turnSpeed_5(float value)
	{
		___turnSpeed_5 = value;
	}

	inline static int32_t get_offset_of_allosaurusCamera_6() { return static_cast<int32_t>(offsetof(AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C, ___allosaurusCamera_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_allosaurusCamera_6() const { return ___allosaurusCamera_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_allosaurusCamera_6() { return &___allosaurusCamera_6; }
	inline void set_allosaurusCamera_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___allosaurusCamera_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allosaurusCamera_6), (void*)value);
	}

	inline static int32_t get_offset_of_cameraAngleX_7() { return static_cast<int32_t>(offsetof(AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C, ___cameraAngleX_7)); }
	inline float get_cameraAngleX_7() const { return ___cameraAngleX_7; }
	inline float* get_address_of_cameraAngleX_7() { return &___cameraAngleX_7; }
	inline void set_cameraAngleX_7(float value)
	{
		___cameraAngleX_7 = value;
	}

	inline static int32_t get_offset_of_cameraAngleY_8() { return static_cast<int32_t>(offsetof(AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C, ___cameraAngleY_8)); }
	inline float get_cameraAngleY_8() const { return ___cameraAngleY_8; }
	inline float* get_address_of_cameraAngleY_8() { return &___cameraAngleY_8; }
	inline void set_cameraAngleY_8(float value)
	{
		___cameraAngleY_8 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_9() { return static_cast<int32_t>(offsetof(AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C, ___cameraDistance_9)); }
	inline float get_cameraDistance_9() const { return ___cameraDistance_9; }
	inline float* get_address_of_cameraDistance_9() { return &___cameraDistance_9; }
	inline void set_cameraDistance_9(float value)
	{
		___cameraDistance_9 = value;
	}
};


// AllosaurusSelectScript
struct AllosaurusSelectScript_t55B7409202A46909BC18D0919BC5D6A4FB005ACA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject AllosaurusSelectScript::allosaurusCamera
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___allosaurusCamera_4;
	// UnityEngine.GameObject[] AllosaurusSelectScript::allosaurus
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___allosaurus_5;

public:
	inline static int32_t get_offset_of_allosaurusCamera_4() { return static_cast<int32_t>(offsetof(AllosaurusSelectScript_t55B7409202A46909BC18D0919BC5D6A4FB005ACA, ___allosaurusCamera_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_allosaurusCamera_4() const { return ___allosaurusCamera_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_allosaurusCamera_4() { return &___allosaurusCamera_4; }
	inline void set_allosaurusCamera_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___allosaurusCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allosaurusCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_allosaurus_5() { return static_cast<int32_t>(offsetof(AllosaurusSelectScript_t55B7409202A46909BC18D0919BC5D6A4FB005ACA, ___allosaurus_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_allosaurus_5() const { return ___allosaurus_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_allosaurus_5() { return &___allosaurus_5; }
	inline void set_allosaurus_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___allosaurus_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allosaurus_5), (void*)value);
	}
};


// ChangeEra
struct ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject ChangeEra::skeleton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___skeleton_4;
	// UnityEngine.GameObject ChangeEra::skinned
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___skinned_5;
	// UnityEngine.GameObject ChangeEra::nowButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___nowButton_6;
	// UnityEngine.GameObject ChangeEra::thenButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___thenButton_7;

public:
	inline static int32_t get_offset_of_skeleton_4() { return static_cast<int32_t>(offsetof(ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF, ___skeleton_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_skeleton_4() const { return ___skeleton_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_skeleton_4() { return &___skeleton_4; }
	inline void set_skeleton_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___skeleton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skeleton_4), (void*)value);
	}

	inline static int32_t get_offset_of_skinned_5() { return static_cast<int32_t>(offsetof(ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF, ___skinned_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_skinned_5() const { return ___skinned_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_skinned_5() { return &___skinned_5; }
	inline void set_skinned_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___skinned_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skinned_5), (void*)value);
	}

	inline static int32_t get_offset_of_nowButton_6() { return static_cast<int32_t>(offsetof(ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF, ___nowButton_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_nowButton_6() const { return ___nowButton_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_nowButton_6() { return &___nowButton_6; }
	inline void set_nowButton_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___nowButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nowButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_thenButton_7() { return static_cast<int32_t>(offsetof(ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF, ___thenButton_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_thenButton_7() const { return ___thenButton_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_thenButton_7() { return &___thenButton_7; }
	inline void set_thenButton_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___thenButton_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thenButton_7), (void*)value);
	}
};


// SimpleAllosaurusCharacter
struct SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Animator SimpleAllosaurusCharacter::allosaurusAnimator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___allosaurusAnimator_4;
	// System.Boolean SimpleAllosaurusCharacter::isGrounded
	bool ___isGrounded_5;
	// System.Boolean SimpleAllosaurusCharacter::jumpUp
	bool ___jumpUp_6;
	// System.Single SimpleAllosaurusCharacter::jumpSpeed
	float ___jumpSpeed_7;
	// System.Single SimpleAllosaurusCharacter::groundCheckOffset
	float ___groundCheckOffset_8;
	// System.Single SimpleAllosaurusCharacter::groundCheckDistance
	float ___groundCheckDistance_9;
	// System.Single SimpleAllosaurusCharacter::runCycleLegOffset
	float ___runCycleLegOffset_10;

public:
	inline static int32_t get_offset_of_allosaurusAnimator_4() { return static_cast<int32_t>(offsetof(SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B, ___allosaurusAnimator_4)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_allosaurusAnimator_4() const { return ___allosaurusAnimator_4; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_allosaurusAnimator_4() { return &___allosaurusAnimator_4; }
	inline void set_allosaurusAnimator_4(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___allosaurusAnimator_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allosaurusAnimator_4), (void*)value);
	}

	inline static int32_t get_offset_of_isGrounded_5() { return static_cast<int32_t>(offsetof(SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B, ___isGrounded_5)); }
	inline bool get_isGrounded_5() const { return ___isGrounded_5; }
	inline bool* get_address_of_isGrounded_5() { return &___isGrounded_5; }
	inline void set_isGrounded_5(bool value)
	{
		___isGrounded_5 = value;
	}

	inline static int32_t get_offset_of_jumpUp_6() { return static_cast<int32_t>(offsetof(SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B, ___jumpUp_6)); }
	inline bool get_jumpUp_6() const { return ___jumpUp_6; }
	inline bool* get_address_of_jumpUp_6() { return &___jumpUp_6; }
	inline void set_jumpUp_6(bool value)
	{
		___jumpUp_6 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_7() { return static_cast<int32_t>(offsetof(SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B, ___jumpSpeed_7)); }
	inline float get_jumpSpeed_7() const { return ___jumpSpeed_7; }
	inline float* get_address_of_jumpSpeed_7() { return &___jumpSpeed_7; }
	inline void set_jumpSpeed_7(float value)
	{
		___jumpSpeed_7 = value;
	}

	inline static int32_t get_offset_of_groundCheckOffset_8() { return static_cast<int32_t>(offsetof(SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B, ___groundCheckOffset_8)); }
	inline float get_groundCheckOffset_8() const { return ___groundCheckOffset_8; }
	inline float* get_address_of_groundCheckOffset_8() { return &___groundCheckOffset_8; }
	inline void set_groundCheckOffset_8(float value)
	{
		___groundCheckOffset_8 = value;
	}

	inline static int32_t get_offset_of_groundCheckDistance_9() { return static_cast<int32_t>(offsetof(SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B, ___groundCheckDistance_9)); }
	inline float get_groundCheckDistance_9() const { return ___groundCheckDistance_9; }
	inline float* get_address_of_groundCheckDistance_9() { return &___groundCheckDistance_9; }
	inline void set_groundCheckDistance_9(float value)
	{
		___groundCheckDistance_9 = value;
	}

	inline static int32_t get_offset_of_runCycleLegOffset_10() { return static_cast<int32_t>(offsetof(SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B, ___runCycleLegOffset_10)); }
	inline float get_runCycleLegOffset_10() const { return ___runCycleLegOffset_10; }
	inline float* get_address_of_runCycleLegOffset_10() { return &___runCycleLegOffset_10; }
	inline void set_runCycleLegOffset_10(float value)
	{
		___runCycleLegOffset_10 = value;
	}
};


// SimpleAllosaurusUserController
struct SimpleAllosaurusUserController_t78C0DD7D74194E7802BE8D8EF95FA4DD4F50A3FE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// SimpleAllosaurusCharacter SimpleAllosaurusUserController::allosaurusCharacter
	SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * ___allosaurusCharacter_4;

public:
	inline static int32_t get_offset_of_allosaurusCharacter_4() { return static_cast<int32_t>(offsetof(SimpleAllosaurusUserController_t78C0DD7D74194E7802BE8D8EF95FA4DD4F50A3FE, ___allosaurusCharacter_4)); }
	inline SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * get_allosaurusCharacter_4() const { return ___allosaurusCharacter_4; }
	inline SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B ** get_address_of_allosaurusCharacter_4() { return &___allosaurusCharacter_4; }
	inline void set_allosaurusCharacter_4(SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * value)
	{
		___allosaurusCharacter_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allosaurusCharacter_4), (void*)value);
	}
};


// changeObject
struct changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject changeObject::skeleton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___skeleton_4;
	// UnityEngine.GameObject changeObject::skinmesh
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___skinmesh_5;
	// UnityEngine.GameObject changeObject::button1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___button1_6;
	// UnityEngine.GameObject changeObject::button2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___button2_7;

public:
	inline static int32_t get_offset_of_skeleton_4() { return static_cast<int32_t>(offsetof(changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF, ___skeleton_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_skeleton_4() const { return ___skeleton_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_skeleton_4() { return &___skeleton_4; }
	inline void set_skeleton_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___skeleton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skeleton_4), (void*)value);
	}

	inline static int32_t get_offset_of_skinmesh_5() { return static_cast<int32_t>(offsetof(changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF, ___skinmesh_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_skinmesh_5() const { return ___skinmesh_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_skinmesh_5() { return &___skinmesh_5; }
	inline void set_skinmesh_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___skinmesh_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skinmesh_5), (void*)value);
	}

	inline static int32_t get_offset_of_button1_6() { return static_cast<int32_t>(offsetof(changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF, ___button1_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_button1_6() const { return ___button1_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_button1_6() { return &___button1_6; }
	inline void set_button1_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___button1_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___button1_6), (void*)value);
	}

	inline static int32_t get_offset_of_button2_7() { return static_cast<int32_t>(offsetof(changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF, ___button2_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_button2_7() const { return ___button2_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_button2_7() { return &___button2_7; }
	inline void set_button2_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___button2_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___button2_7), (void*)value);
	}
};


// myARScript
struct myARScript_tEE07E96B5A96A03A733B48DA96263E6D53BE20E5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject myARScript::obj1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj1_4;
	// UnityEngine.GameObject myARScript::obj2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj2_5;

public:
	inline static int32_t get_offset_of_obj1_4() { return static_cast<int32_t>(offsetof(myARScript_tEE07E96B5A96A03A733B48DA96263E6D53BE20E5, ___obj1_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_obj1_4() const { return ___obj1_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_obj1_4() { return &___obj1_4; }
	inline void set_obj1_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___obj1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___obj1_4), (void*)value);
	}

	inline static int32_t get_offset_of_obj2_5() { return static_cast<int32_t>(offsetof(myARScript_tEE07E96B5A96A03A733B48DA96263E6D53BE20E5, ___obj2_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_obj2_5() const { return ___obj2_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_obj2_5() { return &___obj2_5; }
	inline void set_obj2_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___obj2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___obj2_5), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_tCB3D927F30A1769FAEA216264EE98EFFDA4E5DF2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610  m_Items[1];

public:
	inline AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);

// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Lerp_mBFA4C4D2574C8140AA840273D3E6565D66F6F261 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void UnityEngine.Quaternion::set_eulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Quaternion_set_eulerAngles_m8EC47544D5D909682498552C6A13BD0B13EA9277 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429 (int32_t ___key0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// System.Void AllosaurusCameraScript2::CameraRotationX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2_CameraRotationX_mE123D9BD3EE9294C18901A670AB144EA2B08100B (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, const RuntimeMethod* method);
// System.Void AllosaurusCameraScript2::CameraRotationY()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2_CameraRotationY_m357E608C289CED1F2364C9DE159F574A35EE6FF2 (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<AllosaurusCameraScript>()
inline AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2 * GameObject_GetComponent_TisAllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2_m5A589ED476E3CEB37D4A568C8D5AFB9935865C24 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void SimpleAllosaurusCharacter::GroundedCheck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_GroundedCheck_m1CDE8DBEBAE9F9A78F50ACBF5A09F12236AFCFF8 (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Animator::GetFloat(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Animator_GetFloat_m74C168FD46C780CD153838BDCE77F8B371456D46 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_set_applyRootMotion_m21695E3FF10306945F9498641F6945BB73FC4266 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, bool ___value1, const RuntimeMethod* method);
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA  Animator_GetCurrentAnimatorStateInfo_m562250C74BF8C626B5227FE840D6CB739B5F8314 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___layerIndex0, const RuntimeMethod* method);
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimatorStateInfo_get_normalizedTime_mC951C5D83749FC2AE37DCC75A022383C578F3B40 (AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Repeat_mBAB712BA039DF58DBB1B31B669E502C54F3F13CE (float ___t0, float ___length1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetTrigger(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___force0, int32_t ___mode1, const RuntimeMethod* method);
// UnityEngine.AnimatorClipInfo[] UnityEngine.Animator::GetCurrentAnimatorClipInfo(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimatorClipInfoU5BU5D_tCB3D927F30A1769FAEA216264EE98EFFDA4E5DF2* Animator_GetCurrentAnimatorClipInfo_m03540FE5542A41B213B202CD48D429044E1B8A5A (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___layerIndex0, const RuntimeMethod* method);
// UnityEngine.AnimationClip UnityEngine.AnimatorClipInfo::get_clip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * AnimatorClipInfo_get_clip_m0822D4BB447803A294410A319C812D2D4B946A95 (AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m284670765E1627E43B7B0F5EC811A688EE595091 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, float ___maxDistance2, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<SimpleAllosaurusCharacter>()
inline SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * Component_GetComponent_TisSimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B_mD0D1DB7A2460FDDE4787FA18F91F720B574AFD05 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Void SimpleAllosaurusCharacter::Jump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_Jump_mC664573103DD80D4F003B3E6F1912FBAE4E33486 (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method);
// System.Void SimpleAllosaurusCharacter::Attack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_Attack_m7E968DB183702D8A3BF799C19184A8C61BF6E33E (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (int32_t ___key0, const RuntimeMethod* method);
// System.Void SimpleAllosaurusCharacter::TailAttack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_TailAttack_m72FCDA2FCCD573B50D65C8064142467857C5E503 (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method);
// System.Void SimpleAllosaurusCharacter::Move(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_Move_mFC7CF01A090AC3790636D818176A3B845438949D (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, float ___v0, float ___h1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AllosaurusCameraScript::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript_FixedUpdate_m7B71869DD72D9B21FD6004E8C6F2D3571EB0936F (AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2 * __this, const RuntimeMethod* method)
{
	{
		// transform.position = Vector3.Lerp (transform.position,target.transform.position,Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_target_4();
		NullCheck(L_3);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		float L_6;
		L_6 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_2, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_7, /*hidden argument*/NULL);
		// transform.rotation = Quaternion.Lerp (transform.rotation,target.transform.rotation,Time.deltaTime*turnSpeed);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_10;
		L_10 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_9, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = __this->get_target_4();
		NullCheck(L_11);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_13;
		L_13 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_12, /*hidden argument*/NULL);
		float L_14;
		L_14 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_15 = __this->get_turnSpeed_5();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_16;
		L_16 = Quaternion_Lerp_mBFA4C4D2574C8140AA840273D3E6565D66F6F261(L_10, L_13, ((float)il2cpp_codegen_multiply((float)L_14, (float)L_15)), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_8, L_16, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AllosaurusCameraScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript__ctor_m39E8F2A8432B4BBCCA7816EFD4CF13771681A03B (AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2 * __this, const RuntimeMethod* method)
{
	{
		// public float turnSpeed=.2f;
		__this->set_turnSpeed_5((0.200000003f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AllosaurusCameraScript2::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2_Start_mEE3FF1F4C4C944DE1DE23F8AB0D1FDDB5BAB6A32 (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, const RuntimeMethod* method)
{
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Quaternion arotation = Quaternion.identity;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_0;
		L_0 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		V_0 = L_0;
		// Vector3 eua = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_1 = L_1;
		// eua.y = 360f-cameraAngleY;
		float L_2 = __this->get_cameraAngleY_8();
		(&V_1)->set_y_3(((float)il2cpp_codegen_subtract((float)(360.0f), (float)L_2)));
		// eua.z = 0f;
		(&V_1)->set_z_4((0.0f));
		// eua.x = 180f+cameraAngleX;
		float L_3 = __this->get_cameraAngleX_7();
		(&V_1)->set_x_2(((float)il2cpp_codegen_add((float)(180.0f), (float)L_3)));
		// arotation.eulerAngles = eua;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_1;
		Quaternion_set_eulerAngles_m8EC47544D5D909682498552C6A13BD0B13EA9277((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_0), L_4, /*hidden argument*/NULL);
		// transform.localRotation= arotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6 = V_0;
		NullCheck(L_5);
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_5, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AllosaurusCameraScript2::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2_Update_m961E189273740AC98F940FB573A95F262B9374A0 (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFC6687DC37346CD2569888E29764F727FAF530E0);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetKey (KeyCode.Mouse1)) {
		bool L_0;
		L_0 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(((int32_t)324), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		// cameraAngleY+= Input.GetAxis("Mouse X");
		float L_1 = __this->get_cameraAngleY_8();
		float L_2;
		L_2 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, /*hidden argument*/NULL);
		__this->set_cameraAngleY_8(((float)il2cpp_codegen_add((float)L_1, (float)L_2)));
		// cameraAngleX+= Input.GetAxis("Mouse Y");
		float L_3 = __this->get_cameraAngleX_7();
		float L_4;
		L_4 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, /*hidden argument*/NULL);
		__this->set_cameraAngleX_7(((float)il2cpp_codegen_add((float)L_3, (float)L_4)));
	}

IL_003a:
	{
		// CameraRotationX ();
		AllosaurusCameraScript2_CameraRotationX_mE123D9BD3EE9294C18901A670AB144EA2B08100B(__this, /*hidden argument*/NULL);
		// CameraRotationY ();
		AllosaurusCameraScript2_CameraRotationY_m357E608C289CED1F2364C9DE159F574A35EE6FF2(__this, /*hidden argument*/NULL);
		// if (Input.GetAxis ("Mouse ScrollWheel") > .001f) {
		float L_5;
		L_5 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteralFC6687DC37346CD2569888E29764F727FAF530E0, /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)(0.00100000005f)))))
		{
			goto IL_006b;
		}
	}
	{
		// cameraDistance=cameraDistance+Time.deltaTime;
		float L_6 = __this->get_cameraDistance_9();
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_cameraDistance_9(((float)il2cpp_codegen_add((float)L_6, (float)L_7)));
		// }else if(Input.GetAxis ("Mouse ScrollWheel") <-.001f){
		goto IL_008e;
	}

IL_006b:
	{
		// }else if(Input.GetAxis ("Mouse ScrollWheel") <-.001f){
		float L_8;
		L_8 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteralFC6687DC37346CD2569888E29764F727FAF530E0, /*hidden argument*/NULL);
		if ((!(((float)L_8) < ((float)(-0.00100000005f)))))
		{
			goto IL_008e;
		}
	}
	{
		// cameraDistance=cameraDistance-Time.deltaTime;
		float L_9 = __this->get_cameraDistance_9();
		float L_10;
		L_10 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_cameraDistance_9(((float)il2cpp_codegen_subtract((float)L_9, (float)L_10)));
	}

IL_008e:
	{
		// allosaurusCamera.transform.localPosition = new Vector3 (0f,cameraDistance,-2f*cameraDistance);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = __this->get_allosaurusCamera_6();
		NullCheck(L_11);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_11, /*hidden argument*/NULL);
		float L_13 = __this->get_cameraDistance_9();
		float L_14 = __this->get_cameraDistance_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), (0.0f), L_13, ((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_14)), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_12, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AllosaurusCameraScript2::TargetSet(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2_TargetSet_m7558D6933D60B27E6D325AA06C18CD31C1B99DE0 (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___aTarget0, const RuntimeMethod* method)
{
	{
		// target = aTarget;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___aTarget0;
		__this->set_target_4(L_0);
		// }
		return;
	}
}
// System.Void AllosaurusCameraScript2::CameraRotationX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2_CameraRotationX_mE123D9BD3EE9294C18901A670AB144EA2B08100B (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, const RuntimeMethod* method)
{
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Quaternion arotation = Quaternion.identity;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_0;
		L_0 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		V_0 = L_0;
		// Vector3 eua = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_1 = L_1;
		// eua.y = 360f-cameraAngleY;
		float L_2 = __this->get_cameraAngleY_8();
		(&V_1)->set_y_3(((float)il2cpp_codegen_subtract((float)(360.0f), (float)L_2)));
		// eua.z = 0f;
		(&V_1)->set_z_4((0.0f));
		// eua.x = 180f+cameraAngleX;
		float L_3 = __this->get_cameraAngleX_7();
		(&V_1)->set_x_2(((float)il2cpp_codegen_add((float)(180.0f), (float)L_3)));
		// arotation.eulerAngles = eua;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_1;
		Quaternion_set_eulerAngles_m8EC47544D5D909682498552C6A13BD0B13EA9277((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_0), L_4, /*hidden argument*/NULL);
		// transform.localRotation= arotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6 = V_0;
		NullCheck(L_5);
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_5, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AllosaurusCameraScript2::CameraRotationY()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2_CameraRotationY_m357E608C289CED1F2364C9DE159F574A35EE6FF2 (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, const RuntimeMethod* method)
{
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Quaternion arotation = Quaternion.identity;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_0;
		L_0 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		V_0 = L_0;
		// Vector3 eua = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_1 = L_1;
		// eua.y = 360f-cameraAngleY;
		float L_2 = __this->get_cameraAngleY_8();
		(&V_1)->set_y_3(((float)il2cpp_codegen_subtract((float)(360.0f), (float)L_2)));
		// eua.z = 0f;
		(&V_1)->set_z_4((0.0f));
		// eua.x = 180f+cameraAngleX;
		float L_3 = __this->get_cameraAngleX_7();
		(&V_1)->set_x_2(((float)il2cpp_codegen_add((float)(180.0f), (float)L_3)));
		// arotation.eulerAngles = eua;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_1;
		Quaternion_set_eulerAngles_m8EC47544D5D909682498552C6A13BD0B13EA9277((Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 *)(&V_0), L_4, /*hidden argument*/NULL);
		// transform.localRotation= arotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6 = V_0;
		NullCheck(L_5);
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_5, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AllosaurusCameraScript2::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2_FixedUpdate_m759495FFFABFD3C98CCBB03007A7CD05B145952D (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, const RuntimeMethod* method)
{
	{
		// transform.position = Vector3.Lerp (transform.position,target.transform.position,Time.deltaTime*10f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_target_4();
		NullCheck(L_3);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		float L_6;
		L_6 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_2, L_5, ((float)il2cpp_codegen_multiply((float)L_6, (float)(10.0f))), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AllosaurusCameraScript2::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusCameraScript2__ctor_m754FA1299DD1BE915D90A96C2C75055759A45758 (AllosaurusCameraScript2_t5A53F959080829422AC83BAB02EB48BB7D19880C * __this, const RuntimeMethod* method)
{
	{
		// public float turnSpeed=.2f;
		__this->set_turnSpeed_5((0.200000003f));
		// float cameraAngleX=180f;
		__this->set_cameraAngleX_7((180.0f));
		// public float cameraDistance=3f;
		__this->set_cameraDistance_9((3.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AllosaurusSelectScript::AllosaurusSelect(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusSelectScript_AllosaurusSelect_mE3DB3D2D460A21EC3FB5DE43D40AC64DF458AC72 (AllosaurusSelectScript_t55B7409202A46909BC18D0919BC5D6A4FB005ACA * __this, int32_t ___alloNum0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2_m5A589ED476E3CEB37D4A568C8D5AFB9935865C24_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AllosaurusCameraScript frogcamerascript=allosaurusCamera.GetComponent<AllosaurusCameraScript>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_allosaurusCamera_4();
		NullCheck(L_0);
		AllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2 * L_1;
		L_1 = GameObject_GetComponent_TisAllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2_m5A589ED476E3CEB37D4A568C8D5AFB9935865C24(L_0, /*hidden argument*/GameObject_GetComponent_TisAllosaurusCameraScript_tE163DB5BC7B413FF95859D304CCF59CAE0FFAAC2_m5A589ED476E3CEB37D4A568C8D5AFB9935865C24_RuntimeMethod_var);
		// frogcamerascript.target = allosaurus [alloNum];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = __this->get_allosaurus_5();
		int32_t L_3 = ___alloNum0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_1);
		L_1->set_target_4(L_5);
		// }
		return;
	}
}
// System.Void AllosaurusSelectScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AllosaurusSelectScript__ctor_m4AFA903565D7C25F841DDEFA6F3B74FA9B155ACF (AllosaurusSelectScript_t55B7409202A46909BC18D0919BC5D6A4FB005ACA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ChangeEra::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeEra_Start_m1F6F01AE6822D7BC8B079FB0B3DD373E4FABA722 (ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ChangeEra::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeEra_Update_m24B1EB88F3BDFEA80874E58F6A309BC4C1EC8D9B (ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ChangeEra::changeobject(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeEra_changeobject_m3AA04100B7970C3B26BF21FF50771AD60BC93089 (ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF * __this, int32_t ___era0, const RuntimeMethod* method)
{
	{
		// if (era == 0)
		int32_t L_0 = ___era0;
		if (L_0)
		{
			goto IL_0034;
		}
	}
	{
		// skeleton.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_skeleton_4();
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// skinned.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_skinned_5();
		NullCheck(L_2);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// nowButton.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_nowButton_6();
		NullCheck(L_3);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// thenButton.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_thenButton_7();
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0034:
	{
		// skeleton.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_skeleton_4();
		NullCheck(L_5);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)0, /*hidden argument*/NULL);
		// skinned.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_skinned_5();
		NullCheck(L_6);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)1, /*hidden argument*/NULL);
		// nowButton.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_nowButton_6();
		NullCheck(L_7);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_7, (bool)1, /*hidden argument*/NULL);
		// thenButton.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_thenButton_7();
		NullCheck(L_8);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_8, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ChangeEra::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeEra__ctor_m8446B0A5D73F84733C0C0D36C9003C5D018AE74F (ChangeEra_t4281A65B072CACA6E683015ADAD9009FE480AFCF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleAllosaurusCharacter::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_Start_mE48629CF53725DB9111CE9740D41266353BBAFBE (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// allosaurusAnimator = GetComponent<Animator> ();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0;
		L_0 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set_allosaurusAnimator_4(L_0);
		// }
		return;
	}
}
// System.Void SimpleAllosaurusCharacter::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_Update_mFD4B1A8513B2D61A74DB5B2018D8A9854743E4F2 (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method)
{
	{
		// GroundedCheck ();
		SimpleAllosaurusCharacter_GroundedCheck_m1CDE8DBEBAE9F9A78F50ACBF5A09F12236AFCFF8(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleAllosaurusCharacter::Jump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_Jump_mC664573103DD80D4F003B3E6F1912FBAE4E33486 (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral429F4C6175C380F918ECE9198122F191B47F685D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral724F6EF07C0D3FFFD32A55027218B65FB4F377E1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8FAFA495180941C2D366DAA66DEA6A068EE6CC9F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDACCF1FF5F51F90AD8CA6644E88C06E5B9441FFB);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (isGrounded & allosaurusAnimator.GetFloat ("Forward")>.1f) {
		bool L_0 = __this->get_isGrounded_5();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1 = __this->get_allosaurusAnimator_4();
		NullCheck(L_1);
		float L_2;
		L_2 = Animator_GetFloat_m74C168FD46C780CD153838BDCE77F8B371456D46(L_1, _stringLiteral724F6EF07C0D3FFFD32A55027218B65FB4F377E1, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_0&(int32_t)((((float)L_2) > ((float)(0.100000001f)))? 1 : 0))))
		{
			goto IL_00e8;
		}
	}
	{
		// jumpUp=true;
		__this->set_jumpUp_6((bool)1);
		// isGrounded = false;
		__this->set_isGrounded_5((bool)0);
		// allosaurusAnimator.applyRootMotion = false;
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3 = __this->get_allosaurusAnimator_4();
		NullCheck(L_3);
		Animator_set_applyRootMotion_m21695E3FF10306945F9498641F6945BB73FC4266(L_3, (bool)0, /*hidden argument*/NULL);
		// allosaurusAnimator.SetBool ("Landing",false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4 = __this->get_allosaurusAnimator_4();
		NullCheck(L_4);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_4, _stringLiteral429F4C6175C380F918ECE9198122F191B47F685D, (bool)0, /*hidden argument*/NULL);
		// float runCycle =
		//     Mathf.Repeat(
		//         allosaurusAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime + runCycleLegOffset, 1);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_5 = __this->get_allosaurusAnimator_4();
		NullCheck(L_5);
		AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA  L_6;
		L_6 = Animator_GetCurrentAnimatorStateInfo_m562250C74BF8C626B5227FE840D6CB739B5F8314(L_5, 0, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7;
		L_7 = AnimatorStateInfo_get_normalizedTime_mC951C5D83749FC2AE37DCC75A022383C578F3B40((AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA *)(&V_0), /*hidden argument*/NULL);
		float L_8 = __this->get_runCycleLegOffset_10();
		float L_9;
		L_9 = Mathf_Repeat_mBAB712BA039DF58DBB1B31B669E502C54F3F13CE(((float)il2cpp_codegen_add((float)L_7, (float)L_8)), (1.0f), /*hidden argument*/NULL);
		// if (runCycle < .5){
		if ((!(((double)((double)((double)L_9))) < ((double)(0.5)))))
		{
			goto IL_0091;
		}
	}
	{
		// allosaurusAnimator.SetTrigger ("JumpLeftFoot");
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_10 = __this->get_allosaurusAnimator_4();
		NullCheck(L_10);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_10, _stringLiteral8FAFA495180941C2D366DAA66DEA6A068EE6CC9F, /*hidden argument*/NULL);
		// }else{
		goto IL_00a1;
	}

IL_0091:
	{
		// allosaurusAnimator.SetTrigger ("JumpRightFoot");
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_11 = __this->get_allosaurusAnimator_4();
		NullCheck(L_11);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_11, _stringLiteralDACCF1FF5F51F90AD8CA6644E88C06E5B9441FFB, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		// GetComponent<Rigidbody> ().AddForce ((transform.up - allosaurusAnimator.GetFloat ("Forward") * transform.right) * jumpSpeed, ForceMode.Impulse);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_12;
		L_12 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_13, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_15 = __this->get_allosaurusAnimator_4();
		NullCheck(L_15);
		float L_16;
		L_16 = Animator_GetFloat_m74C168FD46C780CD153838BDCE77F8B371456D46(L_15, _stringLiteral724F6EF07C0D3FFFD32A55027218B65FB4F377E1, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_16, L_18, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_14, L_19, /*hidden argument*/NULL);
		float L_21 = __this->get_jumpSpeed_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_12);
		Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700(L_12, L_22, 1, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		// }
		return;
	}
}
// System.Void SimpleAllosaurusCharacter::Attack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_Attack_m7E968DB183702D8A3BF799C19184A8C61BF6E33E (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral508450355DF2A5BB172C6D21A5C54D7806587D91);
		s_Il2CppMethodInitialized = true;
	}
	{
		// allosaurusAnimator.SetTrigger ("Attack");
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get_allosaurusAnimator_4();
		NullCheck(L_0);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_0, _stringLiteral508450355DF2A5BB172C6D21A5C54D7806587D91, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleAllosaurusCharacter::TailAttack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_TailAttack_m72FCDA2FCCD573B50D65C8064142467857C5E503 (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC91F9EB420C3FF6B64C07666E3FCAD60BE80E5D7);
		s_Il2CppMethodInitialized = true;
	}
	{
		// allosaurusAnimator.SetTrigger ("TailAttack");
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get_allosaurusAnimator_4();
		NullCheck(L_0);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_0, _stringLiteralC91F9EB420C3FF6B64C07666E3FCAD60BE80E5D7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleAllosaurusCharacter::GroundedCheck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_GroundedCheck_m1CDE8DBEBAE9F9A78F50ACBF5A09F12236AFCFF8 (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral429F4C6175C380F918ECE9198122F191B47F685D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral539886CC0D6E626CD59CCCB9A251E15C3F8DC3EE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (jumpUp) {
		bool L_0 = __this->get_jumpUp_6();
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		// if(allosaurusAnimator.GetCurrentAnimatorClipInfo (0) [0].clip.name == "Fly"){
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1 = __this->get_allosaurusAnimator_4();
		NullCheck(L_1);
		AnimatorClipInfoU5BU5D_tCB3D927F30A1769FAEA216264EE98EFFDA4E5DF2* L_2;
		L_2 = Animator_GetCurrentAnimatorClipInfo_m03540FE5542A41B213B202CD48D429044E1B8A5A(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * L_3;
		L_3 = AnimatorClipInfo_get_clip_m0822D4BB447803A294410A319C812D2D4B946A95((AnimatorClipInfo_t758011D6F2B4C04893FCD364DAA936C801FBC610 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4;
		L_4 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, _stringLiteral539886CC0D6E626CD59CCCB9A251E15C3F8DC3EE, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		// jumpUp=false;
		__this->set_jumpUp_6((bool)0);
	}

IL_0037:
	{
		// if (!jumpUp & Physics.Raycast (transform.position + Vector3.up * groundCheckOffset, Vector3.down, groundCheckDistance)) {
		bool L_6 = __this->get_jumpUp_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		float L_10 = __this->get_groundCheckOffset_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_9, L_10, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_8, L_11, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629(/*hidden argument*/NULL);
		float L_14 = __this->get_groundCheckDistance_9();
		bool L_15;
		L_15 = Physics_Raycast_m284670765E1627E43B7B0F5EC811A688EE595091(L_12, L_13, L_14, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)((((int32_t)L_6) == ((int32_t)0))? 1 : 0)&(int32_t)L_15)))
		{
			goto IL_009f;
		}
	}
	{
		// if (!isGrounded) {
		bool L_16 = __this->get_isGrounded_5();
		if (L_16)
		{
			goto IL_009f;
		}
	}
	{
		// allosaurusAnimator.SetBool ("Landing", true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_17 = __this->get_allosaurusAnimator_4();
		NullCheck(L_17);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_17, _stringLiteral429F4C6175C380F918ECE9198122F191B47F685D, (bool)1, /*hidden argument*/NULL);
		// allosaurusAnimator.applyRootMotion = true;
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_18 = __this->get_allosaurusAnimator_4();
		NullCheck(L_18);
		Animator_set_applyRootMotion_m21695E3FF10306945F9498641F6945BB73FC4266(L_18, (bool)1, /*hidden argument*/NULL);
		// isGrounded = true;
		__this->set_isGrounded_5((bool)1);
	}

IL_009f:
	{
		// }
		return;
	}
}
// System.Void SimpleAllosaurusCharacter::Move(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter_Move_mFC7CF01A090AC3790636D818176A3B845438949D (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, float ___v0, float ___h1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral724F6EF07C0D3FFFD32A55027218B65FB4F377E1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9DF28E2C80756BFACBF3215EA21F094B08573DA8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// allosaurusAnimator.SetFloat ("Forward", v);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get_allosaurusAnimator_4();
		float L_1 = ___v0;
		NullCheck(L_0);
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_0, _stringLiteral724F6EF07C0D3FFFD32A55027218B65FB4F377E1, L_1, /*hidden argument*/NULL);
		// allosaurusAnimator.SetFloat ("Turn", h);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2 = __this->get_allosaurusAnimator_4();
		float L_3 = ___h1;
		NullCheck(L_2);
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_2, _stringLiteral9DF28E2C80756BFACBF3215EA21F094B08573DA8, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleAllosaurusCharacter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusCharacter__ctor_m106F2F9D22E7D7779065F484D3AA8D8FCD74B2FD (SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * __this, const RuntimeMethod* method)
{
	{
		// public float jumpSpeed=5f;
		__this->set_jumpSpeed_7((5.0f));
		// public float groundCheckOffset=0.3f;
		__this->set_groundCheckOffset_8((0.300000012f));
		// public float groundCheckDistance=.8f;
		__this->set_groundCheckDistance_9((0.800000012f));
		// public float runCycleLegOffset=.2f;
		__this->set_runCycleLegOffset_10((0.200000003f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleAllosaurusUserController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusUserController_Start_mB474443A89C2AF0419F27DEFCC99ECC275419020 (SimpleAllosaurusUserController_t78C0DD7D74194E7802BE8D8EF95FA4DD4F50A3FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B_mD0D1DB7A2460FDDE4787FA18F91F720B574AFD05_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// allosaurusCharacter = GetComponent<SimpleAllosaurusCharacter> ();
		SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * L_0;
		L_0 = Component_GetComponent_TisSimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B_mD0D1DB7A2460FDDE4787FA18F91F720B574AFD05(__this, /*hidden argument*/Component_GetComponent_TisSimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B_mD0D1DB7A2460FDDE4787FA18F91F720B574AFD05_RuntimeMethod_var);
		__this->set_allosaurusCharacter_4(L_0);
		// }
		return;
	}
}
// System.Void SimpleAllosaurusUserController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusUserController_Update_mCACF53E36C720B25B4FCA4C8B60E47AB76497929 (SimpleAllosaurusUserController_t78C0DD7D74194E7802BE8D8EF95FA4DD4F50A3FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFBC1FBDF3F91C0637B6624C6C526B3718C7E46A2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetButtonDown ("Jump")) {
		bool L_0;
		L_0 = Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF(_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		// allosaurusCharacter.Jump();
		SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * L_1 = __this->get_allosaurusCharacter_4();
		NullCheck(L_1);
		SimpleAllosaurusCharacter_Jump_mC664573103DD80D4F003B3E6F1912FBAE4E33486(L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		// if (Input.GetButtonDown ("Fire1")) {
		bool L_2;
		L_2 = Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF(_stringLiteralFBC1FBDF3F91C0637B6624C6C526B3718C7E46A2, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		// allosaurusCharacter.Attack();
		SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * L_3 = __this->get_allosaurusCharacter_4();
		NullCheck(L_3);
		SimpleAllosaurusCharacter_Attack_m7E968DB183702D8A3BF799C19184A8C61BF6E33E(L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// if (Input.GetKeyDown(KeyCode.T)) {
		bool L_4;
		L_4 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)116), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		// allosaurusCharacter.TailAttack();
		SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * L_5 = __this->get_allosaurusCharacter_4();
		NullCheck(L_5);
		SimpleAllosaurusCharacter_TailAttack_m72FCDA2FCCD573B50D65C8064142467857C5E503(L_5, /*hidden argument*/NULL);
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void SimpleAllosaurusUserController::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusUserController_FixedUpdate_m1A44B37AB4A9859D535AF106E859EA21C5AA135C (SimpleAllosaurusUserController_t78C0DD7D74194E7802BE8D8EF95FA4DD4F50A3FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// float h = Input.GetAxis ("Horizontal");
		float L_0;
		L_0 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		V_0 = L_0;
		// float v = Input.GetAxis ("Vertical");
		float L_1;
		L_1 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, /*hidden argument*/NULL);
		V_1 = L_1;
		// allosaurusCharacter.Move (v,h);
		SimpleAllosaurusCharacter_t0A1DFAEA363B97C9ED8ACD5020D72F46DAFCA96B * L_2 = __this->get_allosaurusCharacter_4();
		float L_3 = V_1;
		float L_4 = V_0;
		NullCheck(L_2);
		SimpleAllosaurusCharacter_Move_mFC7CF01A090AC3790636D818176A3B845438949D(L_2, L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SimpleAllosaurusUserController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAllosaurusUserController__ctor_m265A72B5FF5A0F9A542C111B5D03BA010B969F0E (SimpleAllosaurusUserController_t78C0DD7D74194E7802BE8D8EF95FA4DD4F50A3FE * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void changeObject::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeObject_Start_mA6D0FE91ACEF4C783DD6BDDF6649341A25F62E7F (changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void changeObject::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeObject_Update_m54394008CF220BC1476F8AD41758B40AEACD2602 (changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void changeObject::changeobj(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeObject_changeobj_m74C257914CFF26C2E31107C7B7F82FEB41941F3A (changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF * __this, int32_t ___era0, const RuntimeMethod* method)
{
	{
		// if (era ==0)
		int32_t L_0 = ___era0;
		if (L_0)
		{
			goto IL_0034;
		}
	}
	{
		// skeleton.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_skeleton_4();
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// skinmesh.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_skinmesh_5();
		NullCheck(L_2);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)1, /*hidden argument*/NULL);
		// button1.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_button1_6();
		NullCheck(L_3);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// button2.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_button2_7();
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0034:
	{
		// skeleton.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_skeleton_4();
		NullCheck(L_5);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		// skinmesh.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_skinmesh_5();
		NullCheck(L_6);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)0, /*hidden argument*/NULL);
		// button2.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_button2_7();
		NullCheck(L_7);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_7, (bool)0, /*hidden argument*/NULL);
		// button1.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_button1_6();
		NullCheck(L_8);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_8, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void changeObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeObject__ctor_mE1AE28A0966A1F67176A95B05A596DCB0025E751 (changeObject_t54A0D02DAA38378F7D41D82973504E915F4812FF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void myARScript::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void myARScript_Start_m1AF4C1259173E6E3042AADE130C456C0CC84E85A (myARScript_tEE07E96B5A96A03A733B48DA96263E6D53BE20E5 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void myARScript::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void myARScript_Update_m99415FF401463873F3CCE7139E8C5D9E1DAAAB20 (myARScript_tEE07E96B5A96A03A733B48DA96263E6D53BE20E5 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void myARScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void myARScript__ctor_m0FE83EA99704FFAC2FA66FFCCCDA18DFC5367C7E (myARScript_tEE07E96B5A96A03A733B48DA96263E6D53BE20E5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a1;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a1;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a1;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
