﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AllosaurusCameraScript::FixedUpdate()
extern void AllosaurusCameraScript_FixedUpdate_m7B71869DD72D9B21FD6004E8C6F2D3571EB0936F (void);
// 0x00000002 System.Void AllosaurusCameraScript::.ctor()
extern void AllosaurusCameraScript__ctor_m39E8F2A8432B4BBCCA7816EFD4CF13771681A03B (void);
// 0x00000003 System.Void AllosaurusCameraScript2::Start()
extern void AllosaurusCameraScript2_Start_mEE3FF1F4C4C944DE1DE23F8AB0D1FDDB5BAB6A32 (void);
// 0x00000004 System.Void AllosaurusCameraScript2::Update()
extern void AllosaurusCameraScript2_Update_m961E189273740AC98F940FB573A95F262B9374A0 (void);
// 0x00000005 System.Void AllosaurusCameraScript2::TargetSet(UnityEngine.GameObject)
extern void AllosaurusCameraScript2_TargetSet_m7558D6933D60B27E6D325AA06C18CD31C1B99DE0 (void);
// 0x00000006 System.Void AllosaurusCameraScript2::CameraRotationX()
extern void AllosaurusCameraScript2_CameraRotationX_mE123D9BD3EE9294C18901A670AB144EA2B08100B (void);
// 0x00000007 System.Void AllosaurusCameraScript2::CameraRotationY()
extern void AllosaurusCameraScript2_CameraRotationY_m357E608C289CED1F2364C9DE159F574A35EE6FF2 (void);
// 0x00000008 System.Void AllosaurusCameraScript2::FixedUpdate()
extern void AllosaurusCameraScript2_FixedUpdate_m759495FFFABFD3C98CCBB03007A7CD05B145952D (void);
// 0x00000009 System.Void AllosaurusCameraScript2::.ctor()
extern void AllosaurusCameraScript2__ctor_m754FA1299DD1BE915D90A96C2C75055759A45758 (void);
// 0x0000000A System.Void AllosaurusSelectScript::AllosaurusSelect(System.Int32)
extern void AllosaurusSelectScript_AllosaurusSelect_mE3DB3D2D460A21EC3FB5DE43D40AC64DF458AC72 (void);
// 0x0000000B System.Void AllosaurusSelectScript::.ctor()
extern void AllosaurusSelectScript__ctor_m4AFA903565D7C25F841DDEFA6F3B74FA9B155ACF (void);
// 0x0000000C System.Void SimpleAllosaurusCharacter::Start()
extern void SimpleAllosaurusCharacter_Start_mE48629CF53725DB9111CE9740D41266353BBAFBE (void);
// 0x0000000D System.Void SimpleAllosaurusCharacter::Update()
extern void SimpleAllosaurusCharacter_Update_mFD4B1A8513B2D61A74DB5B2018D8A9854743E4F2 (void);
// 0x0000000E System.Void SimpleAllosaurusCharacter::Jump()
extern void SimpleAllosaurusCharacter_Jump_mC664573103DD80D4F003B3E6F1912FBAE4E33486 (void);
// 0x0000000F System.Void SimpleAllosaurusCharacter::Attack()
extern void SimpleAllosaurusCharacter_Attack_m7E968DB183702D8A3BF799C19184A8C61BF6E33E (void);
// 0x00000010 System.Void SimpleAllosaurusCharacter::TailAttack()
extern void SimpleAllosaurusCharacter_TailAttack_m72FCDA2FCCD573B50D65C8064142467857C5E503 (void);
// 0x00000011 System.Void SimpleAllosaurusCharacter::GroundedCheck()
extern void SimpleAllosaurusCharacter_GroundedCheck_m1CDE8DBEBAE9F9A78F50ACBF5A09F12236AFCFF8 (void);
// 0x00000012 System.Void SimpleAllosaurusCharacter::Move(System.Single,System.Single)
extern void SimpleAllosaurusCharacter_Move_mFC7CF01A090AC3790636D818176A3B845438949D (void);
// 0x00000013 System.Void SimpleAllosaurusCharacter::.ctor()
extern void SimpleAllosaurusCharacter__ctor_m106F2F9D22E7D7779065F484D3AA8D8FCD74B2FD (void);
// 0x00000014 System.Void SimpleAllosaurusUserController::Start()
extern void SimpleAllosaurusUserController_Start_mB474443A89C2AF0419F27DEFCC99ECC275419020 (void);
// 0x00000015 System.Void SimpleAllosaurusUserController::Update()
extern void SimpleAllosaurusUserController_Update_mCACF53E36C720B25B4FCA4C8B60E47AB76497929 (void);
// 0x00000016 System.Void SimpleAllosaurusUserController::FixedUpdate()
extern void SimpleAllosaurusUserController_FixedUpdate_m1A44B37AB4A9859D535AF106E859EA21C5AA135C (void);
// 0x00000017 System.Void SimpleAllosaurusUserController::.ctor()
extern void SimpleAllosaurusUserController__ctor_m265A72B5FF5A0F9A542C111B5D03BA010B969F0E (void);
// 0x00000018 System.Void ChangeEra::Start()
extern void ChangeEra_Start_m1F6F01AE6822D7BC8B079FB0B3DD373E4FABA722 (void);
// 0x00000019 System.Void ChangeEra::Update()
extern void ChangeEra_Update_m24B1EB88F3BDFEA80874E58F6A309BC4C1EC8D9B (void);
// 0x0000001A System.Void ChangeEra::changeobject(System.Int32)
extern void ChangeEra_changeobject_m3AA04100B7970C3B26BF21FF50771AD60BC93089 (void);
// 0x0000001B System.Void ChangeEra::.ctor()
extern void ChangeEra__ctor_m8446B0A5D73F84733C0C0D36C9003C5D018AE74F (void);
// 0x0000001C System.Void SpinFree::Update()
extern void SpinFree_Update_m9745C29D1BD282BB54DE7A4A574F9269AC337E8D (void);
// 0x0000001D System.Void SpinFree::.ctor()
extern void SpinFree__ctor_mF5F6F438E64345EF6914C902D4699776169062D7 (void);
// 0x0000001E System.Void changeObject::Start()
extern void changeObject_Start_mA6D0FE91ACEF4C783DD6BDDF6649341A25F62E7F (void);
// 0x0000001F System.Void changeObject::Update()
extern void changeObject_Update_m54394008CF220BC1476F8AD41758B40AEACD2602 (void);
// 0x00000020 System.Void changeObject::changeobj(System.Int32)
extern void changeObject_changeobj_m74C257914CFF26C2E31107C7B7F82FEB41941F3A (void);
// 0x00000021 System.Void changeObject::.ctor()
extern void changeObject__ctor_mE1AE28A0966A1F67176A95B05A596DCB0025E751 (void);
// 0x00000022 System.Void changeObject1::Start()
extern void changeObject1_Start_m71704707E9D2A6C05907870FFA2DBC5DD2E49A5F (void);
// 0x00000023 System.Void changeObject1::Update()
extern void changeObject1_Update_mF65B8425069299491D3E0D72D928DE9C7C2707B9 (void);
// 0x00000024 System.Void changeObject1::changeObj(System.Int32)
extern void changeObject1_changeObj_m5D9E4F46C2DF4A4C7058E1E1B0A2B56675BE7DD9 (void);
// 0x00000025 System.Void changeObject1::.ctor()
extern void changeObject1__ctor_mCAFA88EF06F33C38A09D5BE7FC7DED72BBD7C3F0 (void);
// 0x00000026 System.Void myARScript::Start()
extern void myARScript_Start_m1AF4C1259173E6E3042AADE130C456C0CC84E85A (void);
// 0x00000027 System.Void myARScript::Update()
extern void myARScript_Update_m99415FF401463873F3CCE7139E8C5D9E1DAAAB20 (void);
// 0x00000028 System.Void myARScript::.ctor()
extern void myARScript__ctor_m0FE83EA99704FFAC2FA66FFCCCDA18DFC5367C7E (void);
static Il2CppMethodPointer s_methodPointers[40] = 
{
	AllosaurusCameraScript_FixedUpdate_m7B71869DD72D9B21FD6004E8C6F2D3571EB0936F,
	AllosaurusCameraScript__ctor_m39E8F2A8432B4BBCCA7816EFD4CF13771681A03B,
	AllosaurusCameraScript2_Start_mEE3FF1F4C4C944DE1DE23F8AB0D1FDDB5BAB6A32,
	AllosaurusCameraScript2_Update_m961E189273740AC98F940FB573A95F262B9374A0,
	AllosaurusCameraScript2_TargetSet_m7558D6933D60B27E6D325AA06C18CD31C1B99DE0,
	AllosaurusCameraScript2_CameraRotationX_mE123D9BD3EE9294C18901A670AB144EA2B08100B,
	AllosaurusCameraScript2_CameraRotationY_m357E608C289CED1F2364C9DE159F574A35EE6FF2,
	AllosaurusCameraScript2_FixedUpdate_m759495FFFABFD3C98CCBB03007A7CD05B145952D,
	AllosaurusCameraScript2__ctor_m754FA1299DD1BE915D90A96C2C75055759A45758,
	AllosaurusSelectScript_AllosaurusSelect_mE3DB3D2D460A21EC3FB5DE43D40AC64DF458AC72,
	AllosaurusSelectScript__ctor_m4AFA903565D7C25F841DDEFA6F3B74FA9B155ACF,
	SimpleAllosaurusCharacter_Start_mE48629CF53725DB9111CE9740D41266353BBAFBE,
	SimpleAllosaurusCharacter_Update_mFD4B1A8513B2D61A74DB5B2018D8A9854743E4F2,
	SimpleAllosaurusCharacter_Jump_mC664573103DD80D4F003B3E6F1912FBAE4E33486,
	SimpleAllosaurusCharacter_Attack_m7E968DB183702D8A3BF799C19184A8C61BF6E33E,
	SimpleAllosaurusCharacter_TailAttack_m72FCDA2FCCD573B50D65C8064142467857C5E503,
	SimpleAllosaurusCharacter_GroundedCheck_m1CDE8DBEBAE9F9A78F50ACBF5A09F12236AFCFF8,
	SimpleAllosaurusCharacter_Move_mFC7CF01A090AC3790636D818176A3B845438949D,
	SimpleAllosaurusCharacter__ctor_m106F2F9D22E7D7779065F484D3AA8D8FCD74B2FD,
	SimpleAllosaurusUserController_Start_mB474443A89C2AF0419F27DEFCC99ECC275419020,
	SimpleAllosaurusUserController_Update_mCACF53E36C720B25B4FCA4C8B60E47AB76497929,
	SimpleAllosaurusUserController_FixedUpdate_m1A44B37AB4A9859D535AF106E859EA21C5AA135C,
	SimpleAllosaurusUserController__ctor_m265A72B5FF5A0F9A542C111B5D03BA010B969F0E,
	ChangeEra_Start_m1F6F01AE6822D7BC8B079FB0B3DD373E4FABA722,
	ChangeEra_Update_m24B1EB88F3BDFEA80874E58F6A309BC4C1EC8D9B,
	ChangeEra_changeobject_m3AA04100B7970C3B26BF21FF50771AD60BC93089,
	ChangeEra__ctor_m8446B0A5D73F84733C0C0D36C9003C5D018AE74F,
	SpinFree_Update_m9745C29D1BD282BB54DE7A4A574F9269AC337E8D,
	SpinFree__ctor_mF5F6F438E64345EF6914C902D4699776169062D7,
	changeObject_Start_mA6D0FE91ACEF4C783DD6BDDF6649341A25F62E7F,
	changeObject_Update_m54394008CF220BC1476F8AD41758B40AEACD2602,
	changeObject_changeobj_m74C257914CFF26C2E31107C7B7F82FEB41941F3A,
	changeObject__ctor_mE1AE28A0966A1F67176A95B05A596DCB0025E751,
	changeObject1_Start_m71704707E9D2A6C05907870FFA2DBC5DD2E49A5F,
	changeObject1_Update_mF65B8425069299491D3E0D72D928DE9C7C2707B9,
	changeObject1_changeObj_m5D9E4F46C2DF4A4C7058E1E1B0A2B56675BE7DD9,
	changeObject1__ctor_mCAFA88EF06F33C38A09D5BE7FC7DED72BBD7C3F0,
	myARScript_Start_m1AF4C1259173E6E3042AADE130C456C0CC84E85A,
	myARScript_Update_m99415FF401463873F3CCE7139E8C5D9E1DAAAB20,
	myARScript__ctor_m0FE83EA99704FFAC2FA66FFCCCDA18DFC5367C7E,
};
static const int32_t s_InvokerIndices[40] = 
{
	3212,
	3212,
	3212,
	3212,
	2554,
	3212,
	3212,
	3212,
	3212,
	2540,
	3212,
	3212,
	3212,
	3212,
	3212,
	3212,
	3212,
	1479,
	3212,
	3212,
	3212,
	3212,
	3212,
	3212,
	3212,
	2540,
	3212,
	3212,
	3212,
	3212,
	3212,
	2540,
	3212,
	3212,
	3212,
	2540,
	3212,
	3212,
	3212,
	3212,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	40,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
